package main

import (
	"fmt"
	"math"
)

func main() {

	var a int = 1
	var b int = 10
	var c int = 5
	var x1 float64 = 0
	var x2 float64 = 0
	var D int = 0
	fmt.Printf("A = ")
	fmt.Scanf("%d \n", &a)

	fmt.Printf("B = ")
	fmt.Scanf("%d \n", &b)

	fmt.Printf("C = ")
	fmt.Scanf("%d \n", &c)

	D = b*b - 4*a*c
	if D > 0 {
		x1 = (float64(b)*(-1) - math.Sqrt(float64(D))) / (2 * float64(a))
		x2 = (float64(b)*(-1) + math.Sqrt(float64(D))) / (2 * float64(a))
		fmt.Println("x1= ", x1)
		fmt.Println("x2= ", x2)
	} else {
		if D == 0 {
			x1 = (float64(b) * (-1)) / (2 * float64(a))
			x2 = x1
			fmt.Println("x1= ", x1)

		} else {
			fmt.Println("Корни отсутствуют")
		}
	}

}
