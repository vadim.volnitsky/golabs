package main

import (
	"fmt"
)

func main() {
	var a1, a2, b1, b2, c1, c2 float32

	fmt.Println("input a1, b1, c1")
	fmt.Scanf("%f %f %f", &a1, &b1, &c1)
	fmt.Println("input a2, b2, c2")
	fmt.Scanf("%f %f %f", &a2, &b2, &c2)

	if float32(a1/a2) == float32(b1/b2) {
		fmt.Println("Определить невозможно")
	} else {
		y := (c2 - a2*c1/a1) / (b2 - a2*b1/a1)
		x := (c1 - b1*y) / a1

		fmt.Printf("x=%.3f y=%.3f\n", x, y)
	}

}
