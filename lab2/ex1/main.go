// lab2 project main.go
package main

import "fmt"

func main() {

	fmt.Println("min=", Min(1, 2, 3))
	fmt.Println("AVG=", My_avg(1, 2, 3))
	var (
		res1 float64
		res2 string
	)
	res1, res2 = Ur(0, 3)
	if res2 == "один" {
		fmt.Println("x=", res1)
	} else {
		fmt.Println(res2)
	}

}

func Ur(k int, b int) (float64, string) {
	//kx+b=0
	if k == 0 && b == 0 {
		return 0, "бессконечное колличество корней"
	} else {
		if k == 0 {
			return 0, "корней нет"
		} else {
			if b == 0 {
				return 0, "один"
			} else {
				return (float64(b) * (-1)) / float64(k), "один"
			}
		}
	}

}
func My_avg(a int, b int, c int) float64 {
	return float64(a+b+c) / 3.0
}
func Min(a int, b int, c int) int {
	if a < b && a < c {
		return a
	}
	if b < a && b < c {
		return b
	}
	if c < a && c < b {
		return c
	}

	return 0

}
