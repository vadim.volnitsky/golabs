// lab2 project main.go
package main

import "fmt"
import my "lab2/my_func"

func main() {

	fmt.Println("min=", my_func.Min(1, 2, 3))
	fmt.Println("AVG=", my_func.My_avg(1, 2, 3))
	var (
		res1 float64
		res2 string
	)
	res1, res2 = my_func.Ur(0, 3)
	if res2 == "один" {
		fmt.Println("x=", res1)
	} else {
		fmt.Println(res2)
	}

}
