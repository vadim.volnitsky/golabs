package my_func

import "testing"

func Testur(t *testing.T) {
	var x int
	var x1 string
	x, x1 = Ur(1, 0)
	var res int
	var res1 string
	res = 0
	res1 = "один"
	if x == res && x1 == res1 {
		t.Errorf("Тест не пройден! Результат %d, а должен быть %s", x, res)
	}
}

func Testmy_avg(t *testing.T) {
	x := My_avg(1, 2, 3)
	res := 2.0

	if x != res {
		t.Errorf("Тест не пройден! Результат %f, а должен быть %f", x, res)
	}
}

func Testmin(t *testing.T) {
	x := Min(1, 2, 3)
	res := 1

	if x != res {
		t.Errorf("Тест не пройден! Результат %d, а должен быть %d", x, res)
	}
}
