package main

import (
	"fmt"
	"math"
	"time"
)

func main() {
	const (
		a = 16807
		c = 0
		m = (1 << 31) - 1
	)

	var arr [3000]float64
	var unic [3001][2]float64
	length := 1
	t := time.Now()

	arr[0] = float64(t.Unix())

	for i := 1; i < 3000; i++ {
		arr[i] = float64(int64(a*arr[i-1]+c) % m)
	}

	for i := 0; i < 3000; i++ {
		arr[i] = arr[i] //% 200
		for j := 0; j < length; j++ {
			if j == length-1 {
				unic[j][0] = float64(arr[i])
				length++
			}
			if unic[j][0] == float64(arr[i]) {
				unic[j][1]++
				break
			}
		}
	}
	fmt.Println("массив")
	fmt.Println(arr)
	//вероятности
	var Mx float64
	var Dx float64
	for i := 0; i < length; i++ {
		unic[i][1] /= 30
		Mx += unic[i][0] * unic[i][1]
		Dx += unic[i][0] * unic[i][0] * unic[i][1]
	}
	fmt.Println("вероятности")
	fmt.Println(unic)
	fmt.Println("математическое ожидание")
	fmt.Println(Mx)
	fmt.Println("дисперсия")
	fmt.Println(Dx)
	fmt.Println("среднеквадратическое отклонение")
	fmt.Println(math.Sqrt(Dx))

}
