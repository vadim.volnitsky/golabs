// Листинг сервера
package main

import (
	"fmt"
	"math"
	"net/http"
	"strconv"
)

func task_1(x int64, y int64) string {
	if (y > x+1 && y > -x+1 && y > 1) || (y < 0 && (-x+1) > y) {
		return "належить"
	} else {
		return "не належить"
	}
}
func task_2(x int64, y int64, z int64) string {
	a := (math.Pow(float64(max(x, y, z)), 2) - math.Pow(2, float64(x))*math.Pow(float64(min(x, y, z)), 3))
	b := (math.Cos(float64(5*x)) + float64(max(x, y, z)/min(x, y, z)))
	return strconv.FormatFloat(a/b, 'f', -1, 32)
}

func max(x int64, y int64, z int64) int64 {
	if x > y && x > z {
		return x
	} else if y > x && y > z {
		return y
	} else {
		return z
	}
}
func min(x int64, y int64, z int64) int64 {
	if x < y && x < z {
		return x
	} else if y < x && y < z {
		return y
	} else {
		return z
	}
}

const (
	page = `<!DOCTYPE HTML>
<html>
<head>
<title>Laboratornaja rabota 4</title>
</head>
	<body>
		<form>
			<p>Task 1</p>
			<lable>x = <input name="x" type = "number"></lable></br>
			<lable>y = <input name="y" type = "number"></lable></br>
			<input type ="submit" name = "task" value="Submit_1">
		</form>
		<form>
			<p>Task 2</p>
			<lable>x = <input name="x" type = "number"></lable></br>
			<lable>y = <input name="y" type = "number"></lable></br>
			<lable>z = <input name="z" type = "number"></lable></br>
			<input type ="submit" name = "task" value="Submit_2">
		</form>
		<form>
			<p>Task 3</p>
			<lable>r = <input name="r" type = "text"></lable></br>
			<input type ="submit" name = "task" value="Submit_3">
		</form>
	</body>
</html>`
)

type Solution string

var HttpSolution1 Solution = ""

func (s Solution) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, page)
	if r.Method == "GET" {
		switch r.FormValue("task") {
		case "Submit_1":
			x, err := strconv.ParseInt(r.FormValue("x"), 10, 32)
			y, err := strconv.ParseInt(r.FormValue("y"), 10, 32)
			if err == nil {
				fmt.Fprintln(w, task_1(x, y))
			} else {
				fmt.Fprintln(w, "Task 1: ошибка")
			}

		case "Submit_2":
			x, err := strconv.ParseInt(r.FormValue("x"), 10, 32)
			y, err := strconv.ParseInt(r.FormValue("y"), 10, 32)
			z, err := strconv.ParseInt(r.FormValue("z"), 10, 32)
			if err == nil {
				fmt.Fprintln(w, task_2(x, y, z))
			} else {
				fmt.Fprintln(w, "Task 2: ошибка")
			}

		case "Submit_3":
			r, err := strconv.ParseFloat(r.FormValue("r"), 32)
			if err == nil && (r == 20 || r == 40 || r == 80) {
				fmt.Fprintf(w, "S = %f", 3.14*r*r)
			} else {
				fmt.Fprintln(w, "Task 3: возможные значения 20, 40 или 80")
			}
		}
	}

}
func main() {
	// Запускаем локальный сервер
	http.ListenAndServe("localhost:80", HttpSolution1)
}
