package main

import (
	"fmt"
	"math/rand"
	"net/http"
)

const (
	pageHeader = `<!DOCTYPE HTML>
 <html>
 <head>
 <title>Задание 1</title>
 <style>
 .error{
 color:#F6F6F6;
 }
 a {display: block;}
 </style>
 </head>`
	anError = `<p class="error">%s</p>`
)

type Solution string

var HttpSolution1 Solution = "Задание1"

func task1() [12]int {
	var arr [12]int
	for i := 0; i < 12; i++ {
		arr[i] = -rand.Intn(25)
	}
	return arr
}

func task2() []string {
	var letter = "qwertyuiopasdfghjklzxcvbnm"
	var slice []string
	slice = make([]string, 125)
	for i := 0; i < 125; i++ {
		slice[i] = string(letter[rand.Intn(len(letter))])
	}
	return slice
}

func task3() [12]float32 {
	var arr [12]float32
	for i := 0; i < 12; i++ {
		arr[i] = -rand.Float32() * 25
	}
	return arr
}

func task4() ([]float32, float32, float32) {
	arr := task3()
	slice := make([]float32, 12)
	var sum, dob float32
	dob = 1
	for i := 0; i < 12; i++ {
		slice[i] = arr[i]
		sum += slice[i]
		dob *= slice[i]
	}
	return slice, sum, dob
}

func (s Solution) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var body string
	switch r.URL.Path {
	case "/":
		body = `<body>
				<a href="/task1">Задание 1</a>
				<a href="/task2">Задание 2</a>
				<a href="/task3">Задание 3</a>
				<a href="/task4">Задание 4</a>
				<a href="/task5">Задание 5</a>
			</body>`
	case "/task1":
		body = `<body>
				<h2>task1</h2>
				<form method="POST"><input type="submit" value="generate"></form>
				<a href="/">Стартовая страница</a>
			</body>`
		break
	case "/task2":
		body = `<body>
			<h2>task2</h2>
			<form method="POST"><input type="submit" value="generate"></form>
			<a href="/">Стартовая страница</a>
			</body>`
		break
	case "/task3":
		body = `<body>
			<h2>task3</h2>
			<form method="POST"><input type="submit" value="generate"></form>
			<a href="/">Початкова сторінка</a>
			</body>`
		break
	case "/task4":
		body = `<body>
				<h2>task4</h2>
				<form method="POST"><input type="submit" value="generate"></form>
				<a href="/">Початкова сторінка</a>
			</body>`
		break
	case "/task5":
		body = `<body>
				<h2>task5</h2>
				<form method="POST"><input type="submit" value="generate"></form>
				<a href="/">Початкова сторінка</a>
			</body>`
		break
	default:
		body = `<body><h1 class="error">Eroor 404</h1></body>`
	}
	fmt.Fprintln(w, pageHeader, body)

	if r.Method == "POST" {
		switch r.URL.Path {
		case "/task1":
			fmt.Fprintln(w, task1())
			break
		case "/task2":
			fmt.Fprint(w, task2())
			break
		case "/task3":
			fmt.Fprintln(w, task3())
			break
		case "/task4":
			arr, sum, dob := task4()
			fmt.Fprintln(w, arr)
			fmt.Fprintf(w, "<br>sum = %f, dob = %f", sum, dob)
			break
		case "/task5":
			student := map[string]string{
				"Фамилия": "Вольницкий",
				"Имя":     "Вадим",
				"Група":   "АТ-23",
			}
			for index, ell := range student {
				fmt.Fprintf(w, "%s: %s<br>", index, ell)
			}
			break
		}
	}
}
func main() {
	// Запускаем локальный сервер
	http.ListenAndServe("localhost:80", HttpSolution1)
}
