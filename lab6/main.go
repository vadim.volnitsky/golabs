// test5b project main.go
package main

import (
	//"fmt"
	"html/template"
	"net/http"
)

type Variables struct {
	Title    string
	Heading  string
	BodyData string
}

const (
	tmplFile = "main.tpl"
	start    = `Задание к лабораторной работе `
	page     = `Данные`
)

type Solution string

var HttpSolution1 Solution = "Задание 1"

func (s Solution) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	myVars := Variables{}

	tmpl, err := template.ParseFiles(tmplFile)
	templates := template.Must(tmpl, err)

	switch r.URL.Path {
	case "/":
		myVars.Title = "Start page"
		myVars.Heading = "Стартовая страница"
		myVars.BodyData = start
		break
	case "/page1":
		myVars.Title = "Page 1"
		myVars.Heading = "Страница 1"
		myVars.BodyData = page

		break
	case "/page2":
		myVars.Title = "Page 2"
		myVars.Heading = "Страница 2"
		myVars.BodyData = page
		break
	case "/page3":
		myVars.Title = "Page 3"
		myVars.Heading = "Страница 3"
		myVars.BodyData = page
		break
	case "/page4":
		myVars.Title = "Page 4"
		myVars.Heading = "Страница 4"
		myVars.BodyData = page
		break
	case "/page5":
		myVars.Title = "Page 5"
		myVars.Heading = "Страница 5"
		myVars.BodyData = page
		break
	}
	templates.ExecuteTemplate(w, tmplFile, myVars)
}

func main() {
	// Запускаем локальный сервер
	http.ListenAndServe("localhost:80", HttpSolution1)
}
