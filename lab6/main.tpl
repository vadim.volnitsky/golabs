<!DOCTYPE HTML>
<html>
<head>
    <title>{{ .Title }}</title>
    <style>
		a{
			display: block;
			text-decoration: none;
			margin: 3px;
			border: 1px solid black;
			backgroud-color: grey; 
		}
		div{
			display: inline-block;
		}
    </style>
</head>
<body>
    <h1>{{ .Heading }}</h1>
    <div style="width:25%">
		<a href="/page1">Page 1</a>
		<a href="/page2">Page 2</a>
		<a href="/page3">Page 3</a>
		<a href="/page4">Page 4</a>
		<a href="/page5">Page 5</a>
		<a href="/">Start Page</a>
	</div>
	<div style="width:74%">{{.BodyData}}</div>
</body>
</html>