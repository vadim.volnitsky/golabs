// main project main.go
package main

import (
	"strconv"

	"github.com/andlabs/ui"
)

func initGUI() {
	window := ui.NewWindow("task1", 640, 280, true)
	ComboPacket := ui.NewCombobox()
	ComboMaterial := ui.NewCombobox()
	CheckUnderWindow := ui.NewCheckbox("підвіконня")
	ResultButton := ui.NewButton("результат")
	Witdh := ui.NewEntry()
	Height := ui.NewEntry()
	ResultLabel := ui.NewLabel("")
	columns := ui.NewHorizontalBox()

	LeftColumn := ui.NewVerticalBox()
	RightColumn := ui.NewVerticalBox()
	//=========================================== left column ===================================
	ComboMaterial.Append("дерево")
	ComboMaterial.Append("метал")
	ComboMaterial.Append("металопластик")
	ComboMaterial.SetSelected(0)

	LeftColumnData := ui.NewHorizontalBox()
	LeftColumnText := ui.NewVerticalBox()
	LeftColumnInput := ui.NewVerticalBox()

	LeftColumnText.Append(ui.NewLabel("Ширина:"), true)
	LeftColumnText.Append(ui.NewLabel("Висота:"), true)
	LeftColumnText.Append(ui.NewLabel("Матеріал:"), true)
	LeftColumnText.SetPadded(true)

	LeftColumnInput.Append(Witdh, true)
	LeftColumnInput.Append(Height, true)
	LeftColumnInput.Append(ComboMaterial, true)
	LeftColumnInput.SetPadded(true)

	LeftColumnData.Append(LeftColumnText, true)
	LeftColumnData.Append(LeftColumnInput, true)

	LeftColumn.Append(ui.NewLabel("Розміри вікна"), false)
	LeftColumn.Append(LeftColumnData, true)
	LeftColumn.Append(ResultLabel, true)
	LeftColumn.SetPadded(true)
	//=========================================== right column ===================================
	ComboPacket.Append("Однокамерний")
	ComboPacket.Append("Двокамерний")
	ComboPacket.SetSelected(0)

	RightColumn.Append(ui.NewLabel("Склопакет"), false)
	RightColumn.Append(ComboPacket, true)
	RightColumn.Append(CheckUnderWindow, true)
	RightColumn.Append(ResultButton, false)
	RightColumn.SetPadded(true)
	//=========================================== main Window ============================
	columns.Append(LeftColumn, false)
	columns.Append(ui.NewHorizontalBox(), true)
	columns.Append(RightColumn, false)

	window.SetChild(columns)
	window.SetMargined(true)

	ResultButton.OnClicked(func(*ui.Button) {
		w, errW := strconv.ParseFloat(Witdh.Text(), 64)
		h, errH := strconv.ParseFloat(Height.Text(), 64)

		if errW != nil {
			ResultLabel.SetText("Invalid Width")
			return
		}

		if errH != nil {
			ResultLabel.SetText("Invalid Height")
			return
		}

		res := w * h

		if ComboMaterial.Selected() == 0 {

			if ComboPacket.Selected() == 0 {
				res *= 0.25
			} else {
				res *= 0.3
			}
		}
		if ComboMaterial.Selected() == 1 {
			if ComboPacket.Selected() == 0 {
				res *= 0.05
			} else {
				res *= 0.1
			}
		}
		if ComboMaterial.Selected() == 2 {
			if ComboPacket.Selected() == 0 {
				res *= 0.15
			} else {
				res *= 0.2
			}
		}
		if CheckUnderWindow.Checked() {
			res += 35
		}
		ResultLabel.SetText("Результат:  " + strconv.FormatFloat(res, 'f', 2, 64) + " грн")
	})

	window.OnClosing(func(*ui.Window) bool {
		ui.Quit()
		return true
	})
	window.Show()
}
func main() {
	err := ui.Main(initGUI)
	if err != nil {
		panic(err)
	}
}
