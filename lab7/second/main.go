// main project main.go
package main

import (
	"strconv"

	"github.com/andlabs/ui"
)

func initGUI() {
	window := ui.NewWindow("task1", 640, 280, true)
	ComboCountry := ui.NewCombobox()
	ComboSeason := ui.NewCombobox()
	CheckGuid := ui.NewCheckbox("гид")
	CheckSuperRoom := ui.NewCheckbox("номер люкс")
	ResultButton := ui.NewButton("результат")
	Days := ui.NewEntry()
	People := ui.NewEntry()
	ResultLabel := ui.NewLabel("")
	columns := ui.NewHorizontalBox()

	LeftColumn := ui.NewVerticalBox()
	RightColumn := ui.NewVerticalBox()
	ComboCountry.Append("Болгария")
	ComboCountry.Append("Германия")
	ComboCountry.Append("Польша")
	ComboCountry.SetSelected(0)

	LeftColumnData := ui.NewHorizontalBox()
	LeftColumnText := ui.NewVerticalBox()
	LeftColumnInput := ui.NewVerticalBox()

	LeftColumnText.Append(ui.NewLabel("Количество путевок:"), true)
	LeftColumnText.Append(ui.NewLabel("Количество дней:"), true)
	LeftColumnText.Append(ui.NewLabel("Материал:"), true)
	LeftColumnText.SetPadded(true)

	LeftColumnInput.Append(Days, true)
	LeftColumnInput.Append(People, true)
	LeftColumnInput.Append(ComboCountry, true)
	LeftColumnInput.SetPadded(true)

	LeftColumnData.Append(LeftColumnText, true)
	LeftColumnData.Append(LeftColumnInput, true)

	LeftColumn.Append(LeftColumnData, true)
	LeftColumn.Append(ResultLabel, true)
	LeftColumn.SetPadded(true)

	ComboSeason.Append("Лето")
	ComboSeason.Append("Зима")
	ComboSeason.SetSelected(0)

	CheckBlock := ui.NewVerticalBox()
	CheckBlock.Append(CheckGuid, false)
	CheckBlock.Append(CheckSuperRoom, false)

	RightColumn.Append(ComboSeason, true)
	RightColumn.Append(CheckBlock, true)
	RightColumn.Append(ResultButton, false)
	RightColumn.SetPadded(true)

	columns.Append(LeftColumn, false)
	columns.Append(ui.NewHorizontalBox(), true)
	columns.Append(RightColumn, false)

	window.SetChild(columns)
	window.SetMargined(true)

	ResultButton.OnClicked(func(*ui.Button) {
		w, errD := strconv.ParseInt(Days.Text(), 10, 64)
		h, errP := strconv.ParseInt(People.Text(), 10, 64)
		if errD != nil && errP != nil {
			ResultLabel.SetText("Invalid input")
			return
		}
		res := float64(w * h)
		switch ComboCountry.Selected() {
		case 0:
			if ComboSeason.Selected() == 0 {
				res *= 100
			} else {
				res *= 150
			}
			break
		case 1:
			if ComboSeason.Selected() == 0 {
				res *= 160
			} else {
				res *= 200
			}
			break
		case 2:
			if ComboSeason.Selected() == 0 {
				res *= 120
			} else {
				res *= 180
			}
			break
		}

		if CheckSuperRoom.Checked() {
			res *= 1.2
		}
		if CheckGuid.Checked() {
			res += float64(w * h * 50)
		}
		ResultLabel.SetText("Результат: " + strconv.FormatFloat(res, 'f', 2, 64) + " $")
	})

	window.OnClosing(func(*ui.Window) bool {
		ui.Quit()
		return true
	})
	window.Show()
}
func main() {
	err := ui.Main(initGUI)
	if err != nil {
		panic(err)
	}
}
